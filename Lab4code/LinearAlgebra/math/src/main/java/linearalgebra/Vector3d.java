package linearalgebra;

class Vector3d
{
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    public double magnitude()
    {
        double m = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return m;
    }

    public double dotProduct(Vector3d v)
    {
        double p = this.x*v.x + this.y*v.y + this.z*v.z;
        return p;
    }

    public Vector3d add(Vector3d v)
    {
        Vector3d s = new Vector3d(this.x+v.x, this.y+v.y, this.z+v.z);
        return s;
    }

    public String toString()
    {
        String s = "Vector: " + this.x + " " + this.y + " " + this.z; 
        return s;
    }
}