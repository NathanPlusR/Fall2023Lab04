package linearalgebra;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Vector3dTests
{
    @Test
    public void echoTest()
    {
        
        Vector3d v = new Vector3d(1, 2, 3);
        org.junit.Assert.assertEquals("Testing get x method's output", 1, v.getX(), 0);
        org.junit.Assert.assertEquals("Testing get y method's output", 2, v.getY(), 0);
        org.junit.Assert.assertEquals("Testing get z method's output", 3, v.getZ(), 0);
    }

}